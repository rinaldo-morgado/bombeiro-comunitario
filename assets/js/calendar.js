$(function() {
  
  var date = new Date();
  var d = date.getDate();
  var m = date.getMonth();
  var y = date.getFullYear();
  
  $('#calendar').fullCalendar({
    header: {
      left: 'prev,next today',
      center: 'title',
      right: 'month,agendaWeek,agendaDay'
    },
    editable: true,
    events: [
      {
        title: '6h - Sgt. Roberto - Garcia',
        start: new Date(y, m, 1)
      },
      {
        title: '4h - Sgt. Roberto - Garcia',
        start: new Date(y, m, 2)
      },
      
      {
        title: '8h - Sgt. Mateus - Fortaleza',
        start: new Date(y, m, 7)
      },
      
      {
        title: '8h - Sgt. Mateus - Fortaleza',
        start: new Date(y, m, 15)
      },
    
      {
        title: '4h - Sgt. Mateus - Garcia',
        start: new Date(y, m, 25)
      },  
    ]
  });

});