<?php include '../config.php';?>


<!DOCTYPE HTML>
<html>
<?php include '../head.php';?>
<body>

<?php include '../menu.php';?>

  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-md-8 col-md-offset-2">
        <div class="jumbotron">
          <div class="row"> 
            <div class="col-md-8">
              <h2>Saldo de horas da Companhia</h2>
            </div>

          </div>     

          <h3>Sgt. Jo�o Mateus</h3>
          <h4>Mensal</h4>
                  
                <h5>Janeiro</h5>
                  <div class="progress procomp progress-striped active">
                    <div class="progress-bar progress-bar-warning" style="width: 100%"> <p class="progressbarmonth">20 horas</p> </div>
                  </div>

                  <h5>Fevereiro</h5>
                  <div class="progress procomp progress-striped active">
                    <div class="progress-bar progress-bar-warning" style="width: 100%"> <p class="progressbarmonth">20 horas</p> </div>
                  </div>

                  <h5>Mar�o</h5>
                  <div class="progress procomp progress-striped active">
                    <div class="progress-bar progress-bar-warning" style="width: 10%"> <p class="progressbarmonth">2 horas</p> </div>
                  </div>

                  <h5>Abril</h5>
                  <div class="progress procomp progress-striped active">
                    <div class="progress-bar" style="width: 25%"> <p class="progressbarmonth">5 horas</p> </div>
                  </div>
                
                  <h5>Maio</h5>
                  <div class="progress procomp progress-striped active">
                    <div class="progress-bar progress-bar-warning" style="width: 100%"> <p class="progressbarmonth">20 horas</p> </div>
                  </div>

                <h5>Junho</h5>
                  <div class="progress procomp progress-striped active">
                    <div class="progress-bar" style="width: 0%"> <p class="progressbarmonth">0 horas</p> </div>
                  </div>
                  
              <h4>Semestre - 01/17</h4>
                  <div class="progress procomp progress-striped active">
                    <<div class="progress-bar" style="width: 55.3%"> <p class="progressbarmonth">67 horas</p> </div>
                  </div>
                  <p class="progressbarmonth">53 horas restantes no semestre.</p>
                

                  <hr>
          




          <h3>Sgt. Roberto</h3>
          <h4>Mensal</h4>
                  
                <h5>Janeiro</h5>
                  <div class="progress procomp progress-striped active">
                    <div class="progress-bar progress-bar-warning" style="width: 50%"> <p class="progressbarmonth">10 horas</p> </div>
                  </div>

                  <h5>Fevereiro</h5>
                  <div class="progress procomp progress-striped active">
                    <div class="progress-bar progress-bar-warning" style="width: 75%"> <p class="progressbarmonth">15 horas</p> </div>
                  </div>

                  <h5>Mar�o</h5>
                  <div class="progress procomp progress-striped active">
                    <div class="progress-bar progress-bar-warning" style="width: 85%"> <p class="progressbarmonth">17 horas</p> </div>
                  </div>

                  <h5>Abril</h5>
                  <div class="progress procomp progress-striped active">
                    <div class="progress-bar" style="width: 100%"> <p class="progressbarmonth">20 horas</p> </div>
                  </div>
                
                  <h5>Maio</h5>
                  <div class="progress procomp progress-striped active">
                    <div class="progress-bar progress-bar-warning" style="width: 50%"> <p class="progressbarmonth">10 horas</p> </div>
                  </div>

                <h5>Junho</h5>
                  <div class="progress procomp progress-striped active">
                    <div class="progress-bar" style="width: 0%"> <p class="progressbarmonth">0 horas</p> </div>
                  </div>

              <h4>Semestre - 01/17</h4>
                  <div class="progress procomp progress-striped active">
                    <<div class="progress-bar" style="width: 60%"> <p class="progressbarmonth">72 horas</p> </div>
                  </div>
                  <p class="progressbarmonth">48 horas restantes no semestre.</p>
                  <hr>




              </div>
            </div>
        </div>
    </div>
</body>
<?php include '../footer.php';?>
</html>
