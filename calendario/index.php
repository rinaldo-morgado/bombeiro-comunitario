<?php include '../config.php';?>


<!DOCTYPE HTML>

<html>

<?php include '../head.php';?>

<body>

<?php include '../menu.php';?>

	<main>
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-12 col-md-offset-0">
					<div class="jumbotron">
							<div class="container">
							    <h5><a href="#" data-toggle="modal" data-target="#contato">Solicita&ccedil;&atilde;o de hor&aacute;rios</a></h5>
								    <!-- Modal -->
						            <?php include '../modal.php';?>
								    <hr>
								<div id="calendar"></div>

 						
						</div>
					</div>
				</div>
			</div>
		
	</main>
</body>
</html>

<?php include '../footer.php';?>