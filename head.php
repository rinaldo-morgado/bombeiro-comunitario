<head>

    <title>QualityPress - Forms</title>
   <!-- <meta charset="utf-8">-->
    <meta http-equiv="content-type" content="text/html; charset=iso-8859-1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Custom CSS -->
    <link href="../assets/css/style.css" rel="stylesheet">

    <!-- jQuery -->
    <script src="//code.jquery.com/jquery-1.10.2.min.js"></script>

    <!--Calend�rio -->
    <link href="../assets/css/calendar.css" rel="stylesheet">
    <script type="text/javascript" src="../assets/js/calendar.js"></script>
     <script type="text/javascript" src="../assets/js/fullcalendar.min.js"></script>
    
    <!-- Mobile support -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="../assets/css/material.indigo-pink.min.css" rel="stylesheet"> 
    

    <!-- Material Design fonts -->
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:300,400,500,700" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- Bootstrap -->
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">

    <!-- Bootstrap Material Design -->
    <link href="../assets/css/bootstrap-material-design.css" rel="stylesheet">
    <link href="../assets/css/ripples.min.css" rel="stylesheet">
    

    <!-- Dropdown.js -->
    <link href="//cdn.rawgit.com/FezVrasta/dropdown.js/master/jquery.dropdown.css" rel="stylesheet">

 
    <script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js"></script>
    
    <!-- Material Design for Bootstrap -->
    <script src="../assets/js/material.js"></script>
    <script src="../assets/js/ripples.min.js"></script>
    <script src="../assets/js/jquery.mask.js"></script>
    
    <!-- Font Awesome-->
    <link href="../assets/css/font-awesome.min.css" rel="stylesheet">


    <!-- Sliders -->
    <script src="//cdnjs.cloudflare.com/ajax/libs/noUiSlider/6.2.0/jquery.nouislider.min.js"></script>

    <!-- Dropdown.js -->
    <script src="../assets/js/jquery.dropdown.js"></script>

    <script>
       
        

        /* Phone mask script*/
        $(document).ready(function(){
           
            var maskTel = function (val) {
                    return val.replace(/\D/g, '').length === 11 ? '+55 (00) 9 0000-0000' : '+55 (00) 0000-00009';
                },
                options = {onKeyPress: function(val, e, field, options) {
                    field.mask(maskTel.apply({}, arguments), options);
                }
                };

            $('.phone_with_ddd').mask(maskTel, options);

            /*$('.phone_with_ddd').mask('+55 (00) 0 0000-0000');*/

            /* dropdown script*/ 
            $("#dropdown-menu select").dropdown();

            //tooltip
            $('[data-toggle="tooltip"]').tooltip();  
    
            /* Material design script*/    
            $.material.init();
        
            /* Alert message script*/
            window.setTimeout(function () {
                $(".alert-success").fadeTo(500, 0).slideUp(500, function () {
                $(this).remove();
                });
            }, 5000);

             $('.cnpj').mask('00.000.000/0000-00', {reverse: false});

             /* M�scara do campo de ordem de servi�o*/
             $('.order').mask('00000', {reverse: false});

             $('body').on('click', '.eye', function() {
                var $inputPassword = $(this).parent().prev();
                if ($inputPassword.attr('type') == 'password') {
                    $inputPassword.attr('type', 'text');
                } else {
                    $inputPassword.attr('type', 'password');
                }
             });


         $('.send').keydown(function(event) {
            // enter has keyCode = 13, change it if you want to use another button
            if (event.keyCode == 13) {
              this.form.submit();
              return false;
            }
          });
        });


    </script>
</head>